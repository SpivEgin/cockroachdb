FROM debian:stretch-slim

ADD ./files/cockroach_all_x64.zip /opt/
ENV CDBTYPE=full
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install --no-install-recommends unzip &&\
    mkdir /opt/cockroach &&\
    mv /opt/cockroach_all_x64.zip /opt/cockroach/cockroach_all_x64.zip && cd /opt/cockroach &&\
    unzip cockroach_all_x64.zip && cp cockroach.$CDBTYPE cockroach && rm cockroach.* &&\
    chmod +x /opt/cockroach/cockroach && ln -s /opt/cockroach/cockroach /bin/cockroach &&\
    apt-get -y install libncurses5 && apt-get -y autoremove && apt-get -y clean &&\
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*